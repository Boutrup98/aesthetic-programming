function Power(x, y) {
	this.x = x;
	this.y = y;
	this.col = color(0);
	this.size = random(5,20);
	this.speed = random(1,3);
	this.xdirection = 1;
	this.ydirection = 1;

	this.exchange = function(other) {
		let d = dist(this.x, this.y, other.x, other.y);
		if (d < this.size + other.size) {
			return true;
		} else {
			return false;
		}
	}

	this.labor = function() {
		noStroke();
		fill(this.col);
		ellipse(this.x, this.y, this.size*2, this.size*2);
	}

	this.development = function() {
		this.x += this.speed * this.xdirection;
		this.y += this.speed * this.ydirection;

		if (this.x > width - this.size || this.x < this.size) {
		this.xdirection *= -1;
		}

		if (this.y > height - this.size || this.y < this.size) {
		this.ydirection *= -1;
		}
	}
}
