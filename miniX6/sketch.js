function preload() {
  myFont = loadFont('assets/RobotoMono-VariableFont_wght.ttf');
}

let sequence1 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let sequence2 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let sequence3 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let index = 0;

let back = '#000000';
let white = '#ffffff';
let fontS = 150;

function setup() {
  createCanvas(400, 400);
  background(back);

  //generate button
  let genBtn = createButton('Generate');
  genBtn.class('gen');
  genBtn.mousePressed(genPressed);

  function genPressed() {
    shuffle(sequence1, true);
    shuffle(sequence2, true);
    shuffle(sequence3, true);
  }
}

function draw() {

  background(back);

  //canvas for emoticon
  fill(0);
  noStroke();

  //left eye
  push();
  textFont(myFont);
  textSize(fontS);
  fill(white);
  translate(125, 180);
  textAlign(CENTER);
  text(sequence1[index], 0, 0);
  pop();

  //right eye
  push();
  textFont(myFont);
  textSize(fontS);
  fill(white);
  translate(275, 180);
  textAlign(CENTER);
  text(sequence2[index], 0, 0);
  pop();

  //mouth
  push();
  textFont(myFont);
  textSize(fontS);
  fill(white);
  translate(200, 330);
  textAlign(CENTER);
  text(sequence3[index], 0, 0);
  pop();
}
