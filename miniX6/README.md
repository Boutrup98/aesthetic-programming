# README - Emojibit_

**Which MiniX do you plan to rework? and what have you changed and why?**
For this miniX i reworked my miniX2. The objective of this miniX was to experiment with various geometric drawing methods and to explore alternatives, particularly with regard to shapes and drawing with colors. Furthermore we were asked to reflect on the politics/aesthetics of emojis on the basis of the assigned texts. I wanted to rework this miniX because I had little time to work on it last time and I found the subject of more versatile and inclusive emojis very interesting. 

![](/miniX6/assets/emoji.png)

In relation to the assigned reading we looked at a piece made by graphic designer David Reinfurt. The piece is called “Multi”. Multi will at any one moment, presents one of 1,728 possible arrangements, each a face built from minimal typographic furniture. Instead of many identical copies from one design, Multi is one original set of instructions constantly producing alternative versions. The artwork is based on Enzo Mari’s “La Mela”. Enzo Mari wanted to draw the perfect apple “He was not looking to draw AN apple, but rather THE apple — a perfect symbol designed for the serial logic of industrial reproduction.” I found this thought of a universal symbol very interesting.

For my rework I wanted to work with the principals of David Reinfurt and create a program that will generate a random emoji made of 3 versatile computational elements. The idea behind coise is that we communicate daily through a virtual interface so why not keep the computer's natural strukture to express emotions. This way there would be a relatively neutral playground for everybody and there would not be the same focus on social and cultural context as well as politics of representation, identity, race, colonialism. The focus on this has come after companies started humanising the emojis.

```
let sequence1 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let sequence2 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let sequence3 = ['.', ';', ':', '.', '-', '+', '*', '=', '≈', '˜'];
let index = 0;
```

I wanted to create a program that had the same functionalities as David Reinfurt “multi” but I also wanted to give the user more options to customize in terms of colors and fonts. This is where I discovered the limitations of p5.js. The library is primarily used for creating artistic generative programs inside the p5 canvas. This creates limitations when trying to create more complex GUI interfaces.

Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX6/

![](/miniX6/assets/200.gif)

-------------------------------------------------------------------------------

Code references:

https://p5js.org/
