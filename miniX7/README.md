## README - Asteroids ##

**Description**

For this miniX the objective was to create a program using object oriented programming as well as an object oriented approach. I decided to reinterpret an old arcade game from the 80s called Asteroids. Asteroids is a space-themed multidirectional shooter arcade game designed by Lyle Rains and Ed Logg released in November 1979. I wanted my reinterpreted version of the game to stay true to the original but also to bring it up to today's standards in a minimalist way. 

I started by defining the objects that I wanted to include: a ship, asteroids, lasers and stars. I wanted to keep the arcade feeling so you control the game with the keyboard. Press “shift” to shoot, “arrow left and right” to move and “arrow up” to boost. The ship is internally hard to control to create a feeling of zero gravity. The game will gradually also get more difficult because more asteroids will spawn randomly. The objective of the game is to shoot the asteroids which will increase your score and ultimately survive as long as possible.

Fuller and Goffey suggest that this Object-Oriented Modelling of the world is a social-technical practice, "compression and abstracting relations operative at different scales of realities, composing new forms of agency." After developing my game I got a new understanding of this statement and the way i interpret things has somewhat changed. Object-Oriented Program can translate in the real world of how we interpret things and it becomes more of an approach rather than a programming practise.

![](/miniX7/assets/screen1.png)

**Reflection**

I put a lot of thought into the design of the program. I wanted it to stay true to the original design of the game and make people feel a form of nostalgia, but I also wanted to make it look as if the game was designed today. I chose a minimalist approach to signify the simplicity of the game and to make it enjoyable to look at. All-in-all I think the design of the program turned out great.

Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX7/

![](/miniX7/assets/200.gif)

-------------------------------------------------------------------------------

Code references:

https://p5js.org/

https://thecodingtrain.com/
