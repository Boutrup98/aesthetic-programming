function Star(){
  this.x = random(-width,width);
  this.y = random(-height,height);
  this.z = random(width);

  this.move = function() {
    this.z -= speed;
    if(this.z < 1){
      this.z = random(width);
    }
  }

  this.edges = function(){
  }

  this.display = function(){
    this.sx = map(this.x/this.z,0,1,0,width);
    this.sy = map(this.y/this.z,0,1,0,height);
    this.r = map(this.z,0,width,5,0);
    strokeWeight(5);
    line(this.sx,this.sy,this.x,this.y);
    ellipse(this.sx,this.sy,this.r);
  }
}
