var mode;
var bg = '#040D24'
var ship;
var asteroids = [];
var lasers = [];
var score;
var stars = []
var num = 800;
var speed;
var vol;

function preload() {
  soundFormats('mp3', 'wav');
  myFont = loadFont('assets/RobotoMono-VariableFont_wght.ttf');
  mainSound = loadSound('assets/bgSound.mp3');
  boostSound = loadSound('assets/boost.wav');
  laserSound = loadSound('assets/laser.wav');
  looseSound = loadSound('assets/loose.wav');
}

function setup() {
  mode = 0;
  score = 0;
  vol = 1;
  createCanvas(windowWidth, windowHeight);
  mainSound.loop();
  ship = new Ship();
  for (var i = 0; i < 5; i++) {
    asteroids.push(new Asteroid());
  }
  for(var i = 0; i < num; i++) {
    stars.push(new Star());
  }

  // button
  let muteBtn = createButton('Mute music');
  muteBtn.class('mute');
  muteBtn.mousePressed(mutePressed);
  muteBtn.position(25, 25);

  function mutePressed() {
    if (mainSound.isPlaying()) {
    mainSound.stop();
    } else {
    mainSound.play();
    }
  }
}

function draw() {
  if (mode==0) {
    push();
    speed = map(mouseY,0,width,0,20);
    background(bg);
    translate(width/2,height/2);
    fill(255);
    noStroke();
    for(var i = 0; i < stars.length; i++){
      stars[i].move();
      stars[i].edges();
      stars[i].display();
    }
    pop();

    fill(255);
    textSize(50);
    textAlign(CENTER);
    textFont(myFont);
    translate(width/2,height/2.5);
    text('ASTEROIDS', 0,0);

    push();
    fill(255);
    textSize(20);
    textAlign(CENTER);
    textFont(myFont);
    text('Press `enter` to start game',0 ,100);
    pop();

    fill(255, 255, 255, 80);
    textSize(15);
    textAlign(CENTER);
    textFont(myFont);
    text('Press `shift` to shoot, `arrow left and right` to move and `arrow up` to boost', 0, height/2.2);

    push();
    fill(255, 255, 255, 60);
    textSize(10);
    textAlign(CENTER);
    textFont(myFont);
    text('Created by Jacob Boutrup', 0, height/1.8);
    pop();
  }

  if (mode==2) {
    push();
    speed = map(mouseY,0,width,0,20);
    background(bg);
    translate(width/2,height/2);
    fill(255);
    noStroke();
    for(var i = 0; i < stars.length; i++){
      stars[i].move();
      stars[i].edges();
      stars[i].display();
    }
    pop();

    fill(255);
    textSize(50);
    textAlign(CENTER);
    textFont(myFont);
    translate(width/2,height/2.5)
    text('GAME OVER', 0,0);

    push();
    fill(255);
    textSize(20);
    textAlign(CENTER);
    textFont(myFont);
    text('Your score: '+ score, 0, 100);
    pop();

    //button
    let backBtn = createButton('Retry');
    backBtn.class('back');
    backBtn.mousePressed(backPressed);

    function backPressed() {
      location.reload();
    }
  }


  if (mode==1) {
    if(random(1)<0.005) {
      asteroids.push(new Asteroid());
    }
    background(bg);

    push();
    translate(width/2,height/2);
    fill(255);
    noStroke();
    for(var i = 0; i < stars.length; i++){
      stars[i].display();
    }
    pop();

    fill(255);
    textSize(20);
    textAlign(LEFT);
    textFont(myFont);
    text('Score: '+ score, 30,70);
      for (var i = 0; i < asteroids.length; i++) {
        if (ship.hits(asteroids[i])) {
          mode = 2;
          looseSound.play();
        }
        asteroids[i].render();
        asteroids[i].update();
        asteroids[i].edges();
      }

      for (var i = lasers.length - 1; i >= 0; i--) {
        lasers[i].render();
        lasers[i].update();
        if (lasers[i].offscreen()) {
          lasers.splice(i, 1);
        } else {
          for (var j = asteroids.length - 1; j >= 0; j--) {
            if (lasers[i].hits(asteroids[j])) {
              if (asteroids[j].r > 10) {
                var newAsteroids = asteroids[j].breakup();
                asteroids = asteroids.concat(newAsteroids);
              }
              asteroids.splice(j, 1);
              lasers.splice(i, 1);
              score += 10; //round(asteroids[j].r)
              break;
            }
          }
        }
      }

      ship.render();
      ship.turn();
      ship.update();
      ship.edges();
    }
  }

  function keyReleased() {
    ship.setRotation(0);
    ship.boosting(false);
    boostSound.stop();
  }

  function keyPressed() {
    if (keyCode == ENTER) {
      mode = 1;
    }
    if (keyCode == '77') {
      masterVolume(0);
    }
    if (keyCode == '16') {
      lasers.push(new Laser(ship.pos, ship.heading));
      laserSound.play();
    } else if (keyCode == RIGHT_ARROW) {
      ship.setRotation(0.1);
    } else if (keyCode == LEFT_ARROW) {
      ship.setRotation(-0.1);
    } else if (keyCode == UP_ARROW) {
      ship.boosting(true);
      boostSound.setVolume(0.4);
      boostSound.play();
    }
  }

  function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }
