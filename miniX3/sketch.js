let loader = 0 //loader start value
let l = 300 //loading bar lenght
let p = -150 //loading bar posistion

//trollface image preload
function preload() {
  img = loadImage('airplane.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(50);
  background(70);
}

function draw() {
  background(70);
  bar();
  barFill();
  loading();
}

//Loading text and precentage
function loading() {
  push();
  fill(255);
  textAlign(CENTER, CENTER);
  textSize(20);
  textFont('monospace')
  text('Loading: '+ loader + '%',width/2, height/2.4);
    //loader starts at 0 and the count speed is defined by the framecount
    if (frameCount % 3 == 0) {
        loader ++;
      }
    //when the loader reach 99 it will reset and a trollface will appear at mouse posistion
    if (loader > 99) {
      loader = 0;
      img.resize(50, 50);
      image(img, mouseX, mouseY);
      }
  pop();
}

//the filling of the loading bar
function barFill() {
  push();
  translate(width/2,height/2);
  let step = frameCount % l;
  noStroke();
  fill(255,255,255);
  rect(p,0,step,22,20);
  pop();
}

//the frame of the loading bar
function bar() {
  push();
  fill(70);
  stroke(255);
  translate(width/2,height/2);
  rect(p, 0, l, 22, 20);
  pop();
}

//Responsive settings
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
