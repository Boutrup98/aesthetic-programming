# miniX3 - README

In “The Techno-Galactic Guide to Software Observation”, Hans Lammerant talks about time in relation to humans and computers. He talks alot about humans' influence on this matter  “...how the actual experience of time is shaped is strongly influenced by all sort of design decisions and implementations, both for humans and machines.” Lammerant also talks a lot about all sorts of operating systems (OS). This got me thinking about MacOS and the process of updating this operating system. This is often a process where the user has no grasp on time and how long the process is going to take. The only indication the user has of time is a small loading bar that often ends up looping.

![](/miniX3/assets/download.png)

This inspired me to highlight the frustrating parts of this process in the most annoying way possible. So for my MiniX3 i created a throbber disguised as a loading bar. It consists of a gray background with a text saying “loading”, a number going from 0-99% and a loading bar filling out accordingly. The frustrating part is that the user expects the page to load when the bar reaches 100% but when it reaches 99% the bar restarts and a little trollface will appear by the mouse. This loop will continue indefinitely. 

![](/miniX3/assets/load.png)

For this miniX i played around different syntaxes, most importantly for the percentage counter and the loading bar. The percentage counter uses multiple “if” statements. The first one makes the number start at 0 and go up by increments in relation to the “frameCount”. The second resets the counter and displays a “trollface” image. For the moving of the loading bar uses a variable to make the bar go up by increments also in relation to the “frameCount”. All-in-all a simple but effective setup. 

 Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX3/

![](/miniX3/assets/200.gif)
