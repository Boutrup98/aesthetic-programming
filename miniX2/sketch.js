var back = 0;
var face = 255;

function setup() {
  createCanvas(560, 560);
  print("hello world");
  noStroke();
}

function draw() {
  background(back);
  fill(face);

  //left eye
  rect(150, 200, 30, 30);
  rect(180, 170, 30, 30);
  rect(210, 200, 30, 30);

  //Right eye
  rect(300, 200, 30, 30);
  rect(330, 170, 30, 30);
  rect(360, 200, 30, 30);

  //mouth
  rect(150, 300, 30, 30);
  rect(180, 330, 30, 30);
  rect(210, 360, 30, 30);
  rect(240, 360, 30, 30);
  rect(270, 360, 30, 30);
  rect(300, 360, 30, 30);
  rect(330, 330, 30, 30);
  rect(360, 300, 30, 30);

  if (mouseButton) {
    back = 255;
    face = 0;

    //mouth
    rect(180, 300, 30, 30);
    rect(210, 300, 30, 30);
    rect(240, 300, 30, 30);
    rect(270, 300, 30, 30);
    rect(300, 300, 30, 30);
    rect(330, 300, 30, 30);
    rect(210, 330, 30, 30);
    rect(240, 330, 30, 30);
    rect(270, 330, 30, 30);
    rect(300, 330, 30, 30);
  }
}
