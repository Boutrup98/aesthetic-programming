# miniX2 - README

I didn’t have much time for my miniX2 and at the start I maybe wanted to do too much. So i created a RUNME showing a canvas with a simple smiling 8-bit style emoji in black and white. When the user then presses the canvas the 8-bit emoji changes to smiling with open mouth. I wanted to play around with the variable syntax and my original idea was to create a program that would randomly generate 8-bit style emojis. When this didn’t work combined with a time pressure I decided to create this simple program showcasing my thoughts.

![](/miniX2/face.png)

I learnt from this experience that I have to settle on an idea more early on, to avoid getting in time trouble at the end of the week. This would also give me more time to research the syntaxes needed to carry out my idea. 

The reason why I wanted to explore the 8-bit style emoji was to keep the computational simplicity of the language. We communicate daily through a virtual interface so why not keep the computer's natural strukture to express emotions. This way there would be a relatively neutral playground for everybody and there would not be the same focus on social and cultural context as well as politics of representation, identity, race, colonialism. The focus on this have come after companies started humanising the emojis 

![](/miniX2/emoji.png)

Thank you for trying my relatively simple program. 😅 I hope you at least had a litlle fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX2/

![](/miniX2/200.gif)
