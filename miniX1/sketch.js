function setup() {
  // canvas
  createCanvas(windowWidth, windowHeight);
  print("hello world");
  background(200);

  // button reset canvas
  button = createButton('Reset canvas');
  button.position(19, 19);
  button.mousePressed(changeBG);

  function changeBG() {
  background(200);
  }

  // button random size
  button = createButton('Size');
  button.position(130, 19);
  button.mousePressed(changeS);

  function changeS() {
  strokeWeight(random([10], [50]));
  }

  // button random color
  button = createButton('Color');
  button.position(185, 19);
  button.mousePressed(changeC);

  function changeC() {
  stroke( random(255), random(255), random(255) );
  }
}

function draw() {
}
  // draw function
  function mouseDragged() {
    line(mouseX,mouseY, pmouseX, pmouseY);
  }
