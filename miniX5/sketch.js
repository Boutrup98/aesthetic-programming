let x = 0;
let y = 0;
let spacing = 10
let b = 0
let w = 255

function setup() {
  createCanvas(210, 210);
  background(w);
}

function draw() {
  noStroke();
  if (random(1) < 0.5) {
    fill(w)
    rect(x,y,spacing, spacing);
    //line(x, y, x + spacing,y + spacing);
  } else {
    fill(b)
    rect(x,y,spacing, spacing);
    //line(x ,y + spacing,x + spacing, y);
  }
  x = x + spacing;
  if (x > width) {
    x = 0;
    y = y + spacing;
  }
  if (y > height) {
    location.reload();
  }

  push();
  fill(w);
  rect(0,0,80,80);
  fill(b);
  rect(0,0,70,70);
  fill(w);
  rect(10,10,50,50);
  fill(b);
  rect(20,20,30,30);

  fill(w);
  rect(130,0,80,80);
  fill(b);
  rect(140,0,70,70);
  fill(w);
  rect(150,10,50,50);
  fill(b);
  rect(160,20,30,30);

  fill(w);
  rect(0,130,80,80);
  fill(b);
  rect(0,140,70,70);
  fill(w);
  rect(10,150,50,50);
  fill(b);
  rect(20,160,30,30);
  pop();
}
