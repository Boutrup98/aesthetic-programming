# README - QRandom

**Warning** - There is a problem with the reload function beacuse it uses a native javascript function - you can download the program and run i localy to make it work :-) 

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**
The most important part of a generative program is some part of autonomy and independence. Jon McCormack et. al. beskriver det således: “In essence, all generative art focuses on the process by which an artwork is made, and this process is required to have a degree of autonomy and independence from the artist who defines it. The degree of autonomy and independence assigned to the computer varies significantly”. In addition to the level of autonomy/independence a generative program is often defined by some degree of rules we see an example of this with the 10PRINT algorithm. 10PRINT is a line of code written in the BASIC programming language and executed on a Commodore 64. The program generates an endless pattern on the screen. The rules defined for this program makes the program randomly draw either a backwards or forwards slash. This makes for emergent behavior within the program because it cannot be predicted by the examination of the system's individual parts.

```
(random(1) <0.5) {
    line(x, y, x+spacing, y+spacing);
} else {
    line(x, y+spacing, x+spacing, y);
```

**What role do rules and processes have in your work?**
For this miniX we were asked to create a generative program based on rules. The program needed to contain one for/while loop and a conditional statement. My initial idea was to create a program that draws upon the seemingly random generative nature of QR-codes. I wasn’t able to make this happen with a while/for loop, but I was able to make a program that endlessly generates seemingly random QR-codes. So while I didn't manage to make this work with a while/for loop I would argue that the program still complies with rules for a generative program. The algorithm is built on the same base as the 10PRINT program and it is defined by rules that randomly draw a black or a white square and everytime it reaches the end of the canvas the program reloads and starts over again.

![](/miniX5/qr.png)

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator”**
My initial idea was that the program would be able to eventually generate QR-codes that would actually store data. When I started to look into how QR-codes work I discovered that these were also defined by a degree of complex rules. You could argue that QR-codes are a form of functional generative software with a highly emergent behavior, because of the unpredictable nature of some parts of the QR-code. Jon McCormack et. al. asks the question: Can a Machine Originate Anything? This is an interesting question and depending on the subject, it can be viewed in many different ways. In relation to the QR-code it would take a human hours to manually draw out a QR-code. A computer would be able to do this in a few seconds but this would not be possible without a human knowing what rules to define in order to make the program work. in relation to this Jon McCormack et. al. describes two common critical points to this question “The first concerns human ability to know or predict the complete behavior of any program. Program behavior, while defined by the program (created by the programmer), typically has a large, sometimes vast, number of executable pathways. This makes it impossible for the programmer to completely understand and predict the outcome of all but the most trivial programs—one reason why software has “bugs.” The second objection arises from the ability of a program to modify itself. Computer programs can be adaptive; they can learn and so initiate new and potentially creative behaviors.”


Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX5/

![](/miniX5/200.gif)

-------------------------------------------------------------------------------

Code references:

https://p5js.org/
