# miniX4 - Your emotions as data

For this miniX we were asked to create a program related to the call for works from the 2015 Transmediale festival. “In a society ruled by algorithms, data is always at play. The drive towards the quantification of everything means that we are all contributing to a state of permanent capture of life into data.” After reading the call I knew that I wanted to create something that highlighted how obscure data can be. I look at the exhibitions from 2015 to get some inspiration. Especially one exhibition from Erica Scourti called "Body Scan" inspired me a lot because it highlights that everything can be data. “Body Scan records through screenshots a process of photographing different parts of the artist’s body with an iphone similar image app that identifies visual information and links it to online data.” 

Link to Body Scan: https://archive.transmediale.de/content/body-scan

My miniX this week is called “Your emotions as data”. It consists of a webcam with face tracking and an emotion tracking system. The program shows the emotions of the user on 6 different parameters: angry, disgusted, fear, sad, surprised and happy. It turns the emotions into data and the interface shows the data as numbers and as a bar chart. The user can test if the program really works by holding their finger in front of the webcam. This will deactivate the face tracker and the data will stop changing in the interface. Another way to test the program is to smile, this will increase the “happy” parameter and confirm that the program is working. 

![](/miniX4/assets/screen1.png)

For this miniX i played around different syntaxes, most importantly the clmtrackr, emotion.classifier and video capture. These are pre-program models but they make for quite a dramatic effect and it underlines the point i wanted to make, that everything can be data. This also ties in with what Shoshana Zuboff states in the documentary “on surveillance capitalism”, here she mentions “We think that the only personal information they have about us is what we’ve given them and we think we can exert some control over what we give them and therefore we think that our calculation and trade-off here is something that is somehow under our control”.

Thank you for trying my program. I hope you had fun with it! 👌 🎉

https://boutrup98.gitlab.io/aesthetic-programming/miniX4/

![](/miniX4/assets/200.gif)

-------------------------------------------------------------------------------

Code references:

https://github.com/stc/face-tracking-p5js

https://openprocessing.org/sketch/1050513
