function preload() {
  myFont = loadFont('assets/RobotoMono-VariableFont_wght.ttf');
}

let capture;
let ctracker;
let emotions;
let predictedEmotions;
let emotionData;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(100);

  //video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.position(0,0);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //Emotion ctracker
  emotions = new emotionClassifier();
  emotions.init(emotionModel);
  emotionData = emotions.getBlank();

	offset = width / 10;
	margin = offset / 5;
}

function draw() {
  background('#1F0AFE');
  image(capture,0,0);

  //point ctracker
  let positions  = ctracker.getCurrentPosition();
  var cp = ctracker.getCurrentParameters();
  predictedEmotions = emotions.meanPredict(cp);
  if (positions.length) {
    for(let i=0; i< positions.length; i++) {
      noStroke();
      fill(255);
      ellipse(positions[i][0], positions[i][1], 3,3);
    }
  }

  //angry,disgusted,fear,sad,surprised,happy
  cellW = (width - offset * 2 - margin * (predictedEmotions.length - 1)) / predictedEmotions.length;
  cellH = height / 4;

  if (emotions) {
    for (var i = 0; i < predictedEmotions.length; i++) {

      let x = map(i, 0, predictedEmotions.length - 1, offset, width - offset - cellW);
      let y = height - margin * 3;

      let h = map(i, 0, predictedEmotions.length, 0, 360);
      let s = sq(predictedEmotions[i].value) * 100;

      //data set
      console.log(predictedEmotions[i].value);
      fill('#1F0AFE');
      rect(windowWidth/2,0,windowWidth/2, 480);
      fill(255);
      for (let m = 0; m <= 300; m+=50) {
        textSize(20);
        textAlign(LEFT);
        text(predictedEmotions[i].value, width/2+50, m+130);
      }

      //heading text
      textSize(40);
      textAlign(CENTER);
      text('Your emotions as data', width/2, 30);
      textFont(myFont);
      fill(255);

      fill(255);
      noStroke();
      rect(x, y, cellW, -predictedEmotions[i].value * cellH);

      push();
      fill(255);
      textAlign(CENTER, CENTER);
      textFont(myFont);
      textSize(16);
      let label = Object.values(predictedEmotions[i])[0];
      text(label.toUpperCase(), x + cellW / 2, y + margin);
      if (predictedEmotions[i].value > 0.5) {
      pop();
      }
    }
  }
}
function windowResized() {
  resizeCanvas(windowWidth, windowHeight)
}
